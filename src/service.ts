// ---------------------------------------------------------------------------------------------------------------------
// Strata Example Service
// ---------------------------------------------------------------------------------------------------------------------

import { hostname } from 'node:os';
import { StrataConfig, StrataService, logging } from '@strata-js/strata';
import { MessageLoggingMiddleware } from '@strata-js/middleware-message-logging';
import configUtil from '@strata-js/util-config';

// Contexts
import testContext from './contexts/test.js';

// Utils
import { getOrCreateClient } from './utils/clientRegistry.js';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('service');

// ---------------------------------------------------------------------------------------------------------------------

// Determine environment
const env = process.env.ENVIRONMENT ?? 'local';
process.env.HOSTNAME = process.env.HOSTNAME ?? hostname();

// Load config
configUtil.load(`./config/${ env }.yml`);

// Set Logging config
logging.setConfig(configUtil.get<StrataConfig>()?.logging ?? {});

// Create the service instance
const service = new StrataService(configUtil.get());

// Register Contexts
service.registerContext('test', testContext);

// Start the service
async function main() : Promise<void>
{
    // Create client
    const client = await getOrCreateClient('StrataExample');

    // Register Middleware
    const loggingMiddleware = new MessageLoggingMiddleware(
        client,
        {
            serviceGroup: {
                toMonitor: 'StrataExample',
                toLogTo: 'StrataQueueMonitor',
            },
        }
    );
    service.useMiddleware(loggingMiddleware);

    // Start the service
    await service.start();
}

// ---------------------------------------------------------------------------------------------------------------------

main()
    .catch((error) =>
    {
        logger.error(`Encountered unhandled exception. Exiting.`, error.stack);
    });

// ---------------------------------------------------------------------------------------------------------------------
