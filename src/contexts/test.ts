// ---------------------------------------------------------------------------------------------------------------------
// Test Context
// ---------------------------------------------------------------------------------------------------------------------

import { Context, StrataClient, addDebug, addMessage, addWarning, logging } from '@strata-js/strata';
import { ExtendedError, ExtendedErrorSafe, ExtendedServiceError, ServiceError } from '../lib/errors.js';
import { getOrCreateClient } from '../utils/clientRegistry.js';

// ---------------------------------------------------------------------------------------------------------------------

const context = new Context();
const logger = logging.getLogger('testContext');

// ---------------------------------------------------------------------------------------------------------------------

async function getClient() : Promise<StrataClient>
{
    return getOrCreateClient('StrataExample');
}

context.registerOperation('echo', async(request) =>
{
    logger.debug('got request:', request.renderRequest());

    addWarning('This is a warning!', 'some_warning');

    addDebug('Another debug message.', 'some_debug');

    // Echo this back to them
    return request.payload;
});

interface EchoAsyncPayload { delay : number }
context.registerOperation<EchoAsyncPayload>('echoAsync', async(request) =>
{
    logger.debug('got request:', request.renderRequest());

    const delay = request.payload.delay ?? 0;

    // Delay things a bit
    await new Promise((resolve) =>
    {
        setTimeout(resolve, delay);
    });

    // Echo this back to them
    return request.payload;
});

context.registerOperation('fail', async(request) =>
{
    logger.debug('got request:', request.renderRequest());

    throw new ServiceError('ALWAYS FAIL REQUEST!!!111');
});

context.registerOperation('failTest', async(request) =>
{
    logger.debug('got request:', request.renderRequest());

    const errorType = request.payload.errorType ?? 'error';

    switch (errorType.toLowerCase())
    {
        case 'error':
            throw new Error('Thrown Type: Error');
        case 'service_error':
            throw new ServiceError('Thrown Type: ServiceError');
        case 'extended_service_error':
            throw new ExtendedServiceError('Thrown Type: ExtendedServiceError');
        case 'extended_error':
            throw new ExtendedError('Thrown Type: ExtendedError');
        case 'extended_error_safe':
            throw new ExtendedErrorSafe('Thrown Type: ExtendedErrorSafe');
        default:
            throw new Error('Well this shouldn\'t happen');
    }
});

context.registerOperation('messageTest', async(request) =>
{
    request.payload.messages.forEach((message) =>
    {
        addMessage(message);
    });

    return {
        status: 'success',
    };
});

context.registerOperation('postTest', async(request) =>
{
    logger.debug('got request:', request.renderRequest());

    const client = await getClient();

    const serviceGroup = request.payload.serviceGroup ?? 'unknown';
    const ctx = request.payload.context ?? 'unknown';
    const operation = request.payload.operation ?? 'unknown';
    const payload = request.payload.payload ?? {};

    await client.post(serviceGroup, ctx, operation, payload);

    return {
        status: 'success',
    };
});

context.registerOperation('chain', async(request) =>
{
    logger.debug('got request:', request.renderRequest());

    const client = await getClient();

    // In order to handle this request, we're going to need to make another request.
    const responses = await Promise.all([
        client.request('example', 'test', 'echoAsync', request.payload),
        client.request('example', 'test', 'echoAsync', request.payload),
        client.request('example', 'test', 'echo', request.payload),
        client.request('example', 'test', 'echo', request.payload),
    ]);

    return { responses: responses.map((env) => env.payload) };
});

// ---------------------------------------------------------------------------------------------------------------------

export default context;

// ---------------------------------------------------------------------------------------------------------------------
