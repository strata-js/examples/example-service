# Strata Example Service
 
This is an example service writting with [Strata.js][strata] to showcase all the features, and try to mimic what a real 
(if simple) service would look like using this technology.
 
## Development

To run this service, simply check it out, run `npm install` and the start it with `npm start`. There's nothing fancy 
here.
 
## Endpoints
 
This service comes with a few endpoints useful for testing
 
### `test` Context
 
* `echo` - A simple endpoint that immediately echos back whatever you pass it.
* `echoAsync` - The same as `echo`, but takes an optional parameter, `delay` that is the number of milliseconds to delay the return.
* `fail` - An operation that always fails, demonstrating how Strata.js handles custom errors.
* `chain` - Demonstrates chaining multiple requests together, and shows how Strata.js tracks request chains.

[strata]: https://gitlab.com/strata-js/strata/
